package com.greece.veganguide.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class CityDto implements Serializable {

    public int id;
    public String name;

    public CityDto(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getInt("term_id");
        name = jsonObject.getString("name");
    }

    @Override
    public String toString() {
        return this.name;            // What to display in the Spinner list.
    }

    public int getCityByName(String name) {
        return this.id;
    }

}
