package com.greece.veganguide.Model;

import com.greece.veganguide.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DtoHelpers {


    // WORK UTILITIES
    public static String unitWorkHoursConverter(String fields) {

        if (fields.isEmpty() || fields == null)
            return null;

        fields = fields.substring(0, fields.lastIndexOf(','));

        if (fields.equals("((enter-hours),(enter-hours),(enter-hours),(enter-hours),(enter-hours),(enter-hours),(enter-hours)")
                || fields.equals("((open-all-day),(open-all-day),(open-all-day),(open-all-day),(open-all-day),(open-all-day),(open-all-day)"))
            return openEverDay();

        String[] array = fields.split("enter-hours", -1);
        JSONObject workObject = new JSONObject();
        try {
            for (int i = 0; i < array.length; i++) {
                String item = array[i];

                if (item.equals("),(")) {
                    workObject.put(dayMap(i), "Open all day");

                } else if (item.contains("open-all-day") || item.contains("closed-all-day")) {
                    String[] subArray = item.split("\\)\\,\\(", -1);
                    for (String subItem : subArray)
                        if (!subItem.equals(""))
                            workObject.put(dayMap(i), setWork(subItem));

                } else if (item.length() == 17 && item.endsWith("(")) {
                    workObject.put(dayMap(i), setWork(item));

                } else if (item.length() == 14 && item.endsWith(")")) {
                    workObject.put(dayMap(i), setWork(item));

                } else if (item.length() == 15 && item.endsWith(")")) {
                    workObject.put(dayMap(i), setWork(item));

                } else if (item.length() == 31 && item.endsWith("(")) {
                    workObject.put(dayMap(i), setWork(item));
                }
            }
        } catch (JSONException e) {
            return null;
        }
        return workObject.toString();
    }

    private static String setWork(String value) {
        if (value.length() == 17 && value.startsWith(",(") && value.endsWith(")),("))
            return value.substring(2, value.length() - 4);
        else if (value.length() == 15 && value.startsWith(",(") && value.endsWith("))"))
            return value.substring(2, value.length() - 2);
        else if (value.length() == 14 && value.startsWith(",(") && value.endsWith(")"))
            return value.substring(2, value.length() - 1);
        else if (value.length() == 31 && value.startsWith(",(") && value.endsWith(")),("))
            return value.substring(2, value.length() - 4).replaceAll("\\)\\)\\,\\(", "-");
        else if (value.contains("closed-all-day"))
            return "Close all day";
        else if (value.contains("open-all-day"))
            return "Open all day";
        else return "--";
    }

    private static String openEverDay() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Sunday", "Open all day");
            jsonObject.put("Monday", "Open all day");
            jsonObject.put("Tuesday", "Open all day");
            jsonObject.put("Wednesday", "Open all day");
            jsonObject.put("Thursday", "Open all day");
            jsonObject.put("Friday", "Open all day");
            jsonObject.put("Saturday", "Open all day");
        } catch (JSONException e) {
            return new JSONObject().toString();
        }
        return jsonObject.toString();
    }

    private static String dayMap(int i) {
        switch (i) {

            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
        }
        return "";
    }


    // SOCIAL UTILITIES
    public static String unitSocialLinksConverter(String fields) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if (fields != null && fields.length() > 0) {
            String string1 = fields.substring(2, fields.length() - 2);
            String[] array = string1.split("\\)\\,\\(", -1);

            for (String item : array) {
                if (!item.equals(","))
                    jsonArray.put(socialTag(item));
            }
        }
        return jsonArray.toString();
    }

    private static JSONObject socialTag(String string) throws JSONException {
        String[] array = string.split(",");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("socialTag", array[0]);
        if (array.length > 1)
            jsonObject.put("link", array[1]);
        else
            jsonObject.put("link", "");
        return jsonObject;
    }

    public static String getLinkByTag(String tag, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++)
            if (jsonArray.getJSONObject(i).getString("socialTag").equals(tag))
                return jsonArray.getJSONObject(i).getString("link");

        return null;
    }


    // TAG UTILITIES
    public static int getVeganTag(String tag) {
        if (tag.length() > 0) {
            String string = tag.substring(1, tag.length() - 1);
            switch (string) {
                case "Vegan Friendly":
                    return R.drawable.vf_tag;
                case "Vegan Vegetarian":
                    return R.drawable.vv_tag;
                case "Vegan":
                    return R.drawable.v_tag;
            }
        }
        return -1;
    }


    // PHONE UTILITIES
    public static String getParsedPhone(String string) {
        if (string != null && !string.isEmpty()) {
            if (string.contains(",") || string.contains("&")) {
                String[] array = string.split("[,&]");
                return array[0];
            } else {
                return string;
            }
        }
        return null;
    }
}
