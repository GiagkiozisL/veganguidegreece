package com.greece.veganguide.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class VeganNewsDto implements Serializable{

    public int id;
    public String veganLink;        // Needs Decode...
    public String status;
    public String title;
    public String hint;
    public String content;          // Needs Parse HTML
    public String mediaUrl;
    public MediaDto mediaDto;

    public VeganNewsDto(JSONObject jsonObject) throws JSONException, UnsupportedEncodingException {
        id = jsonObject.getInt("id");
        veganLink = URLDecoder.decode(jsonObject.getString("link"), "UTF-8");
        status = jsonObject.getString("status");
        title = jsonObject.getJSONObject("title").getString("rendered");
        hint = jsonObject.getJSONObject("excerpt").getString("rendered");
        content = jsonObject.getJSONObject("content").getString("rendered");
        mediaUrl = jsonObject.getJSONObject("_links").getJSONArray("wp:attachment").getJSONObject(0).getString("href");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVeganLink() {
        return veganLink;
    }

    public void setVeganLink(String veganLink) {
        this.veganLink = veganLink;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public MediaDto getMediaDto() {
        return mediaDto;
    }

    public void setMediaDto(MediaDto mediaDto) {
        this.mediaDto = mediaDto;
    }
}
