package com.greece.veganguide.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class ImageDto {

    public String imageUrl;
    public String imageLink;

    public ImageDto(JSONObject jsonObject) throws JSONException {
            imageUrl = jsonObject.getString("image");
            imageLink = jsonObject.getString("link");
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
