package com.greece.veganguide.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Mike on 8/3/2018.
 */

public class UnitDto implements Serializable {

    public int id;
    public String description;
    public String status;
    public String veganLink;
    public String unitLink;
    public String vvf;
    public String title;
    public String moto;
    public String address;
    public String phone;
    public String phoneNumber;
    public String mail;
    public String mediaUrl;
    public String date;
    public String logoUrl;
    public String coverUrl;
    public String workHours;
    public String socialLinks;
    public MediaDto mediaDto;
    public Double lat;
    public Double lng;
    public int featured;

    public UnitDto(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getInt("id");
        description = jsonObject.getJSONObject("fields").getString("_job_description");
        status = jsonObject.getString("status");
        veganLink = jsonObject.getString("link");
        unitLink = jsonObject.getJSONObject("fields").getString("_job_website");
        vvf = jsonObject.getJSONObject("fields").getString("_vegetarian-vegan-friendly-vegan");
        title = jsonObject.getJSONObject("title").getString("rendered");
        phone = jsonObject.getJSONObject("fields").getString("_job_phone");
        phoneNumber = DtoHelpers.getParsedPhone(jsonObject.getJSONObject("fields").getString("_job_phone"));
        mail = jsonObject.getJSONObject("fields").getString("_job_email");
        moto = jsonObject.getJSONObject("fields").getString("_job_tagline");
        logoUrl = jsonObject.getJSONObject("fields").getString("_job_logo");
        coverUrl = jsonObject.getJSONObject("fields").getString("_job_cover");
        date = jsonObject.getJSONObject("fields").getString("_job_date");
        workHours = DtoHelpers.unitWorkHoursConverter(jsonObject.getJSONObject("fields").getString("_work_hours"));
        socialLinks = DtoHelpers.unitSocialLinksConverter(jsonObject.getJSONObject("fields").getString("_links"));
        address = jsonObject.getJSONObject("fields").getString("_job_location");

        if (!jsonObject.getJSONObject("fields").getString("geolocation_lat").isEmpty())
            lat = jsonObject.getJSONObject("fields").getDouble("geolocation_lat");
        else
            lat = null;
        if (!jsonObject.getJSONObject("fields").getString("geolocation_long").isEmpty())
            lng = jsonObject.getJSONObject("fields").getDouble("geolocation_long");
        else
            lng = null;

        if (!jsonObject.getJSONObject("fields").getString("_featuredlisting").equals(""))
            featured = Integer.valueOf(jsonObject.getJSONObject("fields").getString("_featuredlisting"));
        else
            featured = 0;
        mediaUrl = jsonObject.getJSONObject("_links").getJSONArray("wp:attachment").getJSONObject(0).getString("href");
    }

    public int getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getVvf() {
        return vvf;
    }

    public void setVvf(String vvf) {
        this.vvf = vvf;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getWorkHours() {
        return workHours;
    }

    public void setWorkHours(String workHours) {
        this.workHours = workHours;
    }

    public String getSocialLinks() {
        return socialLinks;
    }

    public void setSocialLinks(String socialLinks) {
        this.socialLinks = socialLinks;
    }

    public String getUnitLink() {
        return unitLink;
    }

    public void setUnitLink(String unitLink) {
        this.unitLink = unitLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVeganLink() {
        return veganLink;
    }

    public void setVeganLink(String veganLink) {
        this.veganLink = veganLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoto() {
        return moto;
    }

    public void setMoto(String moto) {
        this.moto = moto;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public MediaDto getMediaDto() {
        return mediaDto;
    }

    public void setMediaDto(MediaDto mediaDto) {
        this.mediaDto = mediaDto;
    }
}