package com.greece.veganguide.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class MediaDto implements Serializable {

    public int id;
    public String thumbnailUrl;     // 100x100
    public String fullImgUrl;       // 306x306

    public MediaDto(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getInt("id");
        thumbnailUrl = jsonObject.getJSONObject("media_details").getJSONObject("sizes").getJSONObject("shop_thumbnail").getString("source_url");
        fullImgUrl = jsonObject.getJSONObject("media_details").getJSONObject("sizes").getJSONObject("full").getString("source_url");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getFullImgUrl() {
        return fullImgUrl;
    }

    public void setFullImgUrl(String fullImgUrl) {
        this.fullImgUrl = fullImgUrl;
    }
}
