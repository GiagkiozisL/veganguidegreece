package com.greece.veganguide.RecyclerViewAdapter.VeganNewsAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.greece.veganguide.Interfaces.VeganNewItemClickListener;
import com.greece.veganguide.R;

public class VeganNewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView imageView;
    TextView unitTitle;
    TextView unitAddress;
    TextView unitMoto;
    private VeganNewItemClickListener veganNewItemClickListener;

    VeganNewsHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        imageView = itemView.findViewById(R.id.imageView);
        unitTitle = itemView.findViewById(R.id.unitTitle);
        unitMoto = itemView.findViewById(R.id.unitMoto);
        unitAddress = itemView.findViewById(R.id.unitAddress);
        unitAddress.setVisibility(View.GONE);
    }

    void setItemClickListener(VeganNewItemClickListener veganNewItemClickListener) {
        this.veganNewItemClickListener = veganNewItemClickListener;
    }

    @Override
    public void onClick(View view) {
        if (this.veganNewItemClickListener != null)
        veganNewItemClickListener.onItemClick(view, getAdapterPosition());
    }

}
