package com.greece.veganguide.RecyclerViewAdapter.VeganNewsAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.greece.veganguide.Activities.WebViewActivity;
import com.greece.veganguide.Interfaces.VeganNewItemClickListener;
import com.greece.veganguide.Model.MediaDto;
import com.greece.veganguide.Model.VeganNewsDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Requests;
import com.greece.veganguide.Utilities.VeganContext;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class VeganNewsViewAdapter extends RecyclerView.Adapter<VeganNewsHolder> {

    private List<VeganNewsDto> veganNewsDtoList;
    private Context activityContext;

    public VeganNewsViewAdapter(Context context, List<VeganNewsDto> veganNewsDto) {
        activityContext = context;
        this.veganNewsDtoList = new ArrayList<>();
        this.veganNewsDtoList.addAll(veganNewsDto);
    }

    @NonNull
    @Override
    public VeganNewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(VeganContext.getContext())
                .inflate(R.layout.news_item, parent, false);

        return new VeganNewsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VeganNewsHolder holder, int position) {
        holder.unitTitle.setText(Html.fromHtml(veganNewsDtoList.get(position).title));
        holder.unitMoto.setText(Html.fromHtml(veganNewsDtoList.get(position).hint));

        Glide.with(activityContext)
                .load(R.drawable.preloader)
                .apply(new RequestOptions().fitCenter())
                .into(holder.imageView);
        holder.imageView.setPadding(32, 32, 32, 32);

        Requests.getMediaByMediaUrl(veganNewsDtoList.get(position).mediaUrl, new Requests.VolleyMediaByMediaUrl() {
            @Override
            public void onSuccess(JSONArray jsonArray) throws JSONException {
                if (jsonArray.length() != 0) {
                    final int position = holder.getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {

                        try {
                            veganNewsDtoList.get(holder.getAdapterPosition()).setMediaDto(new MediaDto(jsonArray.getJSONObject(0)));
                            Glide.with(VeganContext.getContext())
                                    .load(veganNewsDtoList.get(holder.getAdapterPosition()).getMediaDto().thumbnailUrl)
                                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL))
                                    .into(holder.imageView);
                            holder.imageView.setPadding(0,0,0,0);


                            holder.setItemClickListener(new VeganNewItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    final int myPosition = holder.getAdapterPosition();
                                    if (myPosition != RecyclerView.NO_POSITION) {
                                        Intent intent = new Intent(activityContext, WebViewActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("link", veganNewsDtoList.get(position).veganLink);
                                        intent.putExtras(bundle);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        activityContext.startActivity(intent);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            Glide.with(VeganContext.getContext())
                                    .load(R.drawable.follow_vg)
                                    .apply(new RequestOptions().fitCenter())
                                    .into(holder.imageView);
                            holder.imageView.setPadding(24,24,24,24);

                            holder.setItemClickListener(new VeganNewItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    final int myPosition = holder.getAdapterPosition();
                                    if (myPosition != RecyclerView.NO_POSITION) {
                                        Intent intent = new Intent(activityContext, WebViewActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("link", veganNewsDtoList.get(position).veganLink);
                                        intent.putExtras(bundle);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        activityContext.startActivity(intent);
                                    }
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onError(VolleyError error) {
                holder.setItemClickListener(new VeganNewItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final int myPosition = holder.getAdapterPosition();
                        if (myPosition != RecyclerView.NO_POSITION) {
                            Intent intent = new Intent(activityContext, WebViewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("link", veganNewsDtoList.get(position).veganLink);
                            intent.putExtras(bundle);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Toast.makeText(VeganContext.getContext(), "Image unavailable", Toast.LENGTH_LONG).show();
                            activityContext.startActivity(intent);
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return veganNewsDtoList.size();
    }

    public void setFilter(List<VeganNewsDto> filteredUnitDtoList) {
        veganNewsDtoList = new ArrayList<>();
        veganNewsDtoList.addAll(filteredUnitDtoList);
        notifyDataSetChanged();
    }
}
