package com.greece.veganguide.RecyclerViewAdapter.UnitAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.greece.veganguide.Interfaces.UnitItemClickListener;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.VeganContext;


/**
 * Created by james on 12/7/2017.
 */

class UnitViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView imageView;
    TextView unitTitle;
    TextView unitMoto;
    TextView unitAddress;
    ImageView imageTag;
    private UnitItemClickListener unitItemClickListener;

    UnitViewHolder(View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.imageView);
        unitTitle = itemView.findViewById(R.id.unitTitle);
        unitMoto = itemView.findViewById(R.id.unitMoto);
        unitAddress = itemView.findViewById(R.id.unitAddress);
        imageTag = itemView.findViewById(R.id.imageTag);

        itemView.setOnClickListener(this);
    }

    void setItemClickListener(final UnitItemClickListener unitItemClickListener) {
        this.unitItemClickListener = unitItemClickListener;
    }

    @Override
    public void onClick(View view) {
        if (this.unitItemClickListener != null) {
            unitItemClickListener.onItemClick(view, getAdapterPosition());
        } else
            Toast.makeText(VeganContext.getContext(), "View Null", Toast.LENGTH_SHORT).show();
    }
}
