package com.greece.veganguide.RecyclerViewAdapter.UnitAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.greece.veganguide.Activities.UnitInfoActivity;
import com.greece.veganguide.Activities.WebViewActivity;
import com.greece.veganguide.Interfaces.UnitItemClickListener;
import com.greece.veganguide.Model.DtoHelpers;
import com.greece.veganguide.Model.UnitDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.VeganContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 12/7/2017.
 */

public class UnitViewAdapter extends RecyclerView.Adapter<UnitViewHolder> {

    private List<UnitDto> unitDtoList;
    private Context activityContext;

    public UnitViewAdapter(Context context, List<UnitDto> unitDtoList) {
        activityContext = context;
        this.unitDtoList = new ArrayList<>();
        this.unitDtoList.addAll(unitDtoList);
    }

    @NonNull
    @Override
    public UnitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(VeganContext.getContext())
                .inflate(R.layout.unit_item, parent, false);

        return new UnitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UnitViewHolder holder, int position) {
        holder.unitTitle.setText(Html.fromHtml(unitDtoList.get(position).title));
        holder.unitMoto.setText(Html.fromHtml(unitDtoList.get(position).moto));
        holder.unitAddress.setText(Html.fromHtml(unitDtoList.get(position).address));

        Glide.with(activityContext)
                .load(R.drawable.preloader)
                .apply(new RequestOptions().fitCenter())
                .into(holder.imageView);
        holder.imageView.setPadding(32, 32, 32, 32);

        Glide.with(activityContext)
                .load(unitDtoList.get(position).logoUrl)
                .apply(new RequestOptions().fitCenter())
                .into(holder.imageView);
        holder.imageView.setPadding(0, 0, 0, 0);

        if (unitDtoList.get(position).vvf != null && !unitDtoList.get(position).vvf.isEmpty() && Parameters.category != 56)
            if (DtoHelpers.getVeganTag(unitDtoList.get(position).vvf) != -1)
                holder.imageTag.setImageDrawable(VeganContext.getContext().getResources()
                        .getDrawable(DtoHelpers.getVeganTag(unitDtoList.get(position).vvf)));

        holder.setItemClickListener(new UnitItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final int myPosition = holder.getAdapterPosition();
                if (myPosition != RecyclerView.NO_POSITION) {

                    if (Parameters.category == 54 || Parameters.category == 106) {
                        Intent intent = new Intent(activityContext, WebViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("link", unitDtoList.get(position).veganLink);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activityContext.startActivity(intent);

                    } else {
                        Intent intent = new Intent(activityContext, UnitInfoActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("unitDto", unitDtoList.get(myPosition));
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activityContext.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return unitDtoList.size();
    }

    public void setFilter(List<UnitDto> filteredUnitDtoList) {
        unitDtoList = new ArrayList<>();
        unitDtoList.addAll(filteredUnitDtoList);
        notifyDataSetChanged();
    }

}
