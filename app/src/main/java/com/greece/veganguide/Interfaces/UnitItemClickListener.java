package com.greece.veganguide.Interfaces;

import android.view.View;

/**
 * Created by James on 2/24/2018.
 */

public interface UnitItemClickListener {
    void onItemClick(View view, int position);
}
