package com.greece.veganguide.Interfaces;

import android.view.View;

public interface VeganNewItemClickListener {
    void onItemClick(View view, int position);
}
