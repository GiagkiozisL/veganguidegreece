package com.greece.veganguide.Utilities;

import android.app.Application;
import android.content.Context;

/**
 * Created by James on 2/24/2018.
 */

public class VeganContext extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}
