package com.greece.veganguide.Utilities;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

public class SystemUtils {

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) VeganContext.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

}
