package com.greece.veganguide.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.greece.veganguide.Model.ImageDto;
import com.greece.veganguide.Model.UnitDto;
import com.greece.veganguide.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class UnitUtilities {

    public static int DrawableCategoryMap() {
        if (Parameters.category == null)
            return -1;

        switch (Parameters.category) {
            case 50:
                return R.drawable.eat_primary;
            case 51:
                return R.drawable.shop_primary;
            case 52:
                return R.drawable.eshop_primary;
            case 53:
                return R.drawable.beauty_primary;
            case 54:
                return R.drawable.events_primary;
            case 56:
                return R.drawable.doctor_primary;
            case 0:
                return R.drawable.news_primary;
            case 106:
                return R.drawable.blog_primary;
        }
        return -1;
    }

    public static LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);

            if (address == null)
                return null;

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p1;
    }

    public static List<UnitDto> eventSortList(List<UnitDto> firstList) {
        List<UnitDto> featuredList = new ArrayList<>();
        List<UnitDto> othersList = new ArrayList<>();

        for (UnitDto unit : firstList)
            if (unit.getFeatured() != 0)
                featuredList.add(unit);
            else
                othersList.add(unit);

        if (!othersList.isEmpty()) {
            @SuppressLint("SimpleDateFormat") final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Collections.sort(othersList, new Comparator<UnitDto>() {
                @Override
                public int compare(UnitDto unit1, UnitDto unit2) {
                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                    try {
                        return simpleDateFormat.parse(unit1.date).compareTo(simpleDateFormat.parse(unit2.date));
                    } catch (ParseException e) {
                       return 0;
                    }
                }
            });
        }

        featuredList.addAll(othersList);
       return featuredList;
    }

    public static ImageDto getSliderImageUrl(int index) {
       if (Parameters.slideImageList.get(index) != null)
           try {
               return Parameters.slideImageList.get(index);
           }catch (Exception e) {
               return null;
           }
       return null;
    }

}
