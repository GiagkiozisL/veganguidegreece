package com.greece.veganguide.Utilities;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by James on 3/7/2018.
 */

public class Requests {

    public static void getAllCities(final VolleyCallbackAllCities volleyCallbackAllCities) {

        String url = "http://veganguidegreece.com/wp-json/wp/v2/all-terms";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            volleyCallbackAllCities.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackAllCities.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;charset=utf-8");
                headers.put("Accept", "application/json;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/json;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(VeganContext.getContext());
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);
    }

    public static void getUnitsByCategoryId(final int categoryId, final VolleyCallbackUnitsByCategoryId volleyCallbackUnitsByCategoryId) {

        String url = "http://veganguidegreece.com//wp-json/wp/v2/job-listings?job-categories=" + categoryId + "&per_page=1000000";

        System.out.println(url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            volleyCallbackUnitsByCategoryId.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackUnitsByCategoryId.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;charset=utf-8");
                headers.put("Accept", "application/json;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/json;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonArrayRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(VeganContext.getContext());
        jsonArrayRequest.setShouldCache(false);
        queue.add(jsonArrayRequest);
    }

    public static void getMediaByMediaUrl(final String mediaUrl, final VolleyMediaByMediaUrl volleyMediaByMediaUrl) {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, mediaUrl, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            volleyMediaByMediaUrl.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyMediaByMediaUrl.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;charset=utf-8");
                headers.put("Accept", "application/json;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/json;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonArrayRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(VeganContext.getContext());
        jsonArrayRequest.setShouldCache(false);
        queue.add(jsonArrayRequest);
    }

    public static void getVeganNews(final VolleyCallbackVeganNews volleyCallbackVeganNews) {
        String url = "http://veganguidegreece.com/wp-json/wp/v2/posts/";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            volleyCallbackVeganNews.onSuccess(response);
                        } catch (JSONException | UnsupportedEncodingException e)  {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackVeganNews.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;charset=utf-8");
                headers.put("Accept", "application/json;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/json;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonArrayRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(VeganContext.getContext());
        jsonArrayRequest.setShouldCache(false);
        queue.add(jsonArrayRequest);
    }

    public static void getSliderImages(final VolleyCallbackSlider volleyCallbackSlider) {
        String url = "http://veganguidegreece.com/api.php/wp_revslider_slides?filter=slider_id,eq,2";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallbackSlider.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackSlider.onError(error);
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;charset=utf-8");
                headers.put("Accept", "application/json;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/json;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(VeganContext.getContext());
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);
    }



    public interface VolleyCallbackAllCities {
        void onSuccess(JSONObject jsonObject) throws JSONException;
        void onError(VolleyError error);
    }

    public interface VolleyCallbackUnitsByCategoryId {
        void onSuccess(JSONArray jsonArray) throws JSONException;
        void onError(VolleyError error);
    }

    public interface VolleyMediaByMediaUrl {
        void onSuccess(JSONArray jsonArray) throws JSONException;
        void onError(VolleyError error);
    }

    public interface VolleyCallbackVeganNews {
        void onSuccess(JSONArray jsonArray) throws JSONException, UnsupportedEncodingException;
        void onError(VolleyError error);
    }

    public interface VolleyCallbackSlider {
        void onSuccess(JSONObject jsonObject);
        void onError(VolleyError error);
    }

}
