package com.greece.veganguide.Utilities;

import com.greece.veganguide.Model.CityDto;
import com.greece.veganguide.Model.ImageDto;

import java.util.ArrayList;
import java.util.List;

public class Parameters {

    public static String city;
    public static int cityIndex;
    public static Integer category;
    public static String categoryTitle;
    public static List<CityDto> cityDtoList = new ArrayList<>();
    public static List<ImageDto> slideImageList = new ArrayList<>();
}
