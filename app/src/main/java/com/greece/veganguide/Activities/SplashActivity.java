package com.greece.veganguide.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.greece.veganguide.Model.CityDto;
import com.greece.veganguide.Model.ImageDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class SplashActivity extends AppCompatActivity {

    private List<CityDto> cityDtoList = new ArrayList<>();
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestData();
            }
        }, 3000);
    }

    private void requestData() {
        Requests.getAllCities(new Requests.VolleyCallbackAllCities() {
            @Override
            public void onSuccess(JSONObject jsonObject) throws JSONException {
                JSONObject region = jsonObject.getJSONObject("region");
                Iterator<?> keys = region.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (region.get(key) instanceof JSONObject) {
                        CityDto cityDto = new CityDto((JSONObject) region.get(key));
                        cityDtoList.add(cityDto);
                    }
                }

                Parameters.cityDtoList.clear();
                Parameters.cityDtoList = cityDtoList;

                Requests.getSliderImages(new Requests.VolleyCallbackSlider() {
                    @Override
                    public void onSuccess(JSONObject jsonObject) {
                        try {
                            Parameters.slideImageList.clear();
                            JSONObject object = jsonObject.getJSONObject("wp_revslider_slides");
                            JSONArray records = object.getJSONArray("records");

                            for (int i = 0; i < records.length(); i++) {
                                JSONArray array = records.getJSONArray(i);

                                JSONObject fields = new JSONObject(array.get(3).toString());
                                ImageDto image = new ImageDto(fields);

                                Parameters.slideImageList.add(image);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //ToDo Handle sliderException
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        Intent intent = new Intent(SplashActivity.this, ErrorActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(SplashActivity.this, VeganLabelsActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(VolleyError error) {
                Intent intent = new Intent(SplashActivity.this, ErrorActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
