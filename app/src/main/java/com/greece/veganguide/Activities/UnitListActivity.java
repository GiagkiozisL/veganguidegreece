package com.greece.veganguide.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.greece.veganguide.Model.ImageDto;
import com.greece.veganguide.Model.UnitDto;
import com.greece.veganguide.Model.VeganNewsDto;
import com.greece.veganguide.R;
import com.greece.veganguide.RecyclerViewAdapter.UnitAdapter.UnitViewAdapter;
import com.greece.veganguide.RecyclerViewAdapter.VeganNewsAdapter.VeganNewsViewAdapter;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.Requests;
import com.greece.veganguide.Utilities.UnitUtilities;
import com.greece.veganguide.Utilities.VeganContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UnitListActivity extends AppCompatActivity {

    final List<UnitDto> unitDtoList = new ArrayList<>();
    final List<VeganNewsDto> veganNewsDtoList = new ArrayList<>();
    public MenuItem searchItem;
    Handler h = new Handler();
    int delay = 5 * 1000;
    Runnable runnable;
    private UnitViewAdapter unitViewAdapter;
    private VeganNewsViewAdapter veganNewsViewAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    private TextView noResult;
    private SearchView searchView;
    private ImageView banner;
    private int imageQueue = 0;
    private ImageDto imageDto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_list);
        setTitle(Parameters.categoryTitle);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mRecyclerView = findViewById(R.id.unitRecycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        progressBar = findViewById(R.id.progressBar);
        noResult = findViewById(R.id.noResultText);
        banner = findViewById(R.id.listBanner);

        progressBar.setVisibility(View.VISIBLE);
        noResult.setVisibility(View.GONE);

        if (Parameters.category != 0)
            downloadUnits(Parameters.category);
        else
            downloadVeganNews();

        sliderFunction();

        banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adWebView();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        searchItem = menu.findItem(R.id.menu_search);

        searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getResources().getString(R.string.searchHint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (unitViewAdapter != null) {
                    final List<UnitDto> filteredResults = dataUnitFilter(newText);
                    unitViewAdapter.setFilter(filteredResults);
                    return true;
                } else if (veganNewsViewAdapter != null) {
                    final List<VeganNewsDto> filteredResults = dataVeganNewsFilter(newText);
                    veganNewsViewAdapter.setFilter(filteredResults);
                    return true;
                } else {
                    return false;
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.events:
                Parameters.category = 54;
                Parameters.categoryTitle = getResources().getString(R.string.categoryEvents);
                super.recreate();
                return true;
            case R.id.blogs:
                Parameters.category = 106;
                Parameters.categoryTitle = getResources().getString(R.string.categoryBlogs);
                super.recreate();
                return true;
            case R.id.vegan_news:
                Parameters.category = 0;
                Parameters.categoryTitle = getResources().getString(R.string.categoryNews);
                super.recreate();
                return true;
            case R.id.advertise:
                webIntent(R.id.advertise);
                return true;
            case R.id.contact:
                webIntent(R.id.contact);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0)
            resetMenu();
    }

    @Override
    protected void onResume() {
        h.postDelayed(new Runnable() {
            public void run() {
                sliderFunction();
                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    private void resetMenu() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(
                INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        if (searchView != null)
            if (MenuItemCompat.expandActionView(searchItem))
                MenuItemCompat.collapseActionView(searchItem);
    }

    private void downloadUnits(int categoryId) {
        unitDtoList.clear();
        Requests.getUnitsByCategoryId(categoryId, new Requests.VolleyCallbackUnitsByCategoryId() {
            @Override
            public void onSuccess(JSONArray jsonArray) throws JSONException {
                for (int i = 0; i < jsonArray.length(); i++) {
                    UnitDto unit = new UnitDto(jsonArray.getJSONObject(i));

                    if (unit.status.equals("publish"))
                        if (Parameters.category == 52 || Parameters.category == 106 || Parameters.category == 54)
                            unitDtoList.add(unit);
                        else if (unit.getAddress().contains(Parameters.city))
                            unitDtoList.add(unit);
                }

                if (unitDtoList.size() != 0) {

                    Collections.sort(unitDtoList, new Comparator<UnitDto>() {
                        @Override
                        public int compare(UnitDto unit1, UnitDto unit2) {
                            // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                            // true values first
                            return Integer.compare(unit2.getFeatured(), unit1.getFeatured());
                        }
                    });

                    // If Category Whats on / Events
                    if (Parameters.category == 54)
                        unitViewAdapter = new UnitViewAdapter(VeganContext.getContext(), UnitUtilities.eventSortList(unitDtoList));
                    else
                        unitViewAdapter = new UnitViewAdapter(VeganContext.getContext(), unitDtoList);


                    mRecyclerView.setAdapter(unitViewAdapter);
                    mRecyclerView.scrollToPosition(0);
                    progressBar.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    noResult.setVisibility(View.VISIBLE);
                    noResult.setText(getResources().getString(R.string.no_results_found));
                }
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(VeganContext.getContext(), getResources().getString(R.string.volley_error), Toast.LENGTH_LONG).show();
                noResult.setVisibility(View.VISIBLE);
                noResult.setText(getResources().getString(R.string.connection_msg));
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void downloadVeganNews() {
        veganNewsDtoList.clear();
        Requests.getVeganNews(new Requests.VolleyCallbackVeganNews() {
            @Override
            public void onSuccess(JSONArray jsonArray) throws JSONException, UnsupportedEncodingException {
                for (int i = 0; i < jsonArray.length(); i++) {
                    VeganNewsDto vegan = new VeganNewsDto(jsonArray.getJSONObject(i));
                    if (vegan.status.equals("publish"))
                        veganNewsDtoList.add(vegan);
                }

                if (veganNewsDtoList.size() != 0) {
                    veganNewsViewAdapter = new VeganNewsViewAdapter(VeganContext.getContext(), veganNewsDtoList);
                    mRecyclerView.setAdapter(veganNewsViewAdapter);
                    mRecyclerView.scrollToPosition(0);
                    veganNewsViewAdapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    noResult.setVisibility(View.VISIBLE);
                    noResult.setText(getResources().getString(R.string.no_results_found));
                }
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(VeganContext.getContext(), getResources().getString(R.string.volley_error), Toast.LENGTH_LONG).show();
                noResult.setVisibility(View.VISIBLE);
                noResult.setText(getResources().getString(R.string.connection_msg));
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public List<UnitDto> dataUnitFilter(CharSequence constraint) {
        List<UnitDto> filteredUnitDtoList = new ArrayList<>();
        constraint = constraint.toString().toLowerCase();
        if (constraint.toString().length() > 0) {
            for (UnitDto unit : unitDtoList) {
                if (unit.title.toLowerCase().contains(constraint) || unit.address.toLowerCase().contains(constraint)) {
                    filteredUnitDtoList.add(unit);
                }
            }
        } else {
            return unitDtoList;
        }
        return filteredUnitDtoList;
    }

    public List<VeganNewsDto> dataVeganNewsFilter(CharSequence constraint) {
        List<VeganNewsDto> filteredVeganNewsDtoList = new ArrayList<>();
        constraint = constraint.toString().toLowerCase();
        if (constraint.toString().length() > 0) {
            for (VeganNewsDto veganNews : veganNewsDtoList) {
                if (veganNews.title.toLowerCase().contains(constraint)) {
                    filteredVeganNewsDtoList.add(veganNews);
                }
            }
        } else {
            return veganNewsDtoList;
        }
        return filteredVeganNewsDtoList;
    }

    private void webIntent(int id) {
        Intent intent = new Intent(UnitListActivity.this, WebViewActivity.class);
        if (id == R.id.advertise) {
            intent.putExtra("link", "http://veganguidegreece.com/advertised/");
        } else if (id == R.id.contact) {
            intent.putExtra("link", "http://veganguidegreece.com/contact-us/");
        }
        startActivity(intent);
    }

    private void sliderFunction() {
        if (Parameters.slideImageList.size() == 0) {
            Requests.getSliderImages(new Requests.VolleyCallbackSlider() {
                @Override
                public void onSuccess(JSONObject jsonObject) {
                    try {
                        Parameters.slideImageList.clear();
                        JSONObject object = jsonObject.getJSONObject("wp_revslider_slides");
                        JSONArray records = object.getJSONArray("records");
                        for (int i = 0; i < records.length(); i++) {
                            JSONArray array = records.getJSONArray(i);
                            JSONObject fields = new JSONObject(array.get(3).toString());
                            ImageDto image = new ImageDto(fields);

                            Parameters.slideImageList.add(image);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //ToDo Handle sliderException
                    }
                }

                @Override
                public void onError(VolleyError error) {
                }
            });

        } else if (Parameters.slideImageList.size() - 1 > imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue++;

        } else if (Parameters.slideImageList.size() - 1 == imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue = 0;

        } else {
            imageQueue = 0;
        }

        if (imageDto != null)
            if (imageDto.imageUrl != null)
                Glide.with(VeganContext.getContext())
                        .load(imageDto.imageUrl)
                        .into(banner);
    }

    private void adWebView() {
        if (imageDto != null) {
            if (imageDto.imageLink != null && !imageDto.imageLink.isEmpty()) {
                Intent intent = new Intent(UnitListActivity.this, WebViewActivity.class);
                intent.putExtra("link", imageDto.imageLink);
                startActivity(intent);
            } else
                Toast.makeText(VeganContext.getContext(), getResources().getString(R.string.unavailable_ad_link), Toast.LENGTH_SHORT).show();
        }
    }
}
