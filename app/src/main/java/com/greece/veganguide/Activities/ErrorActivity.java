package com.greece.veganguide.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.greece.veganguide.R;

public class ErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
    }
}
