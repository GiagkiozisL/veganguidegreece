package com.greece.veganguide.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.greece.veganguide.Model.CityDto;
import com.greece.veganguide.Model.ImageDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.Requests;
import com.greece.veganguide.Utilities.SystemUtils;
import com.greece.veganguide.Utilities.UnitUtilities;
import com.greece.veganguide.Utilities.VeganContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LocationActivity extends AppCompatActivity {

    AutoCompleteTextView spinner;
    Button nextBtn;
    List<String> cities = new ArrayList<>();
    Handler h = new Handler();
    int delay = 5 * 1000;
    Runnable runnable;
    private ImageView banner;
    private int imageQueue = 0;
    private ImageDto imageDto = null;
    private boolean locationBoolean = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setTitle(R.string.location);

        spinner = findViewById(R.id.locationSpinner);
        nextBtn = findViewById(R.id.nextButton);
        banner = findViewById(R.id.locationBanner);

        assert Parameters.cityDtoList != null;
        for (CityDto cityDto : Parameters.cityDtoList)
            cities.add(cityDto.name);

        if (Parameters.cityIndex != -1)
            spinner.setSelection(Parameters.cityIndex);

        ArrayAdapter<String> adapterAuto = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, cities);
        spinner.setAdapter(adapterAuto);
        spinner.setThreshold(1);

        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                locationBoolean = true;
                try {
                    SystemUtils.hideSoftKeyboard(LocationActivity.this);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locationBoolean) {
                    Parameters.city = spinner.getText().toString().trim();
                    createIntent();
                    System.out.println(Parameters.city);
                } else
                    Toast.makeText(LocationActivity.this, "Please select a city from list...", Toast.LENGTH_SHORT).show();
            }
        });

        sliderFunction();

        banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adWebView();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(LocationActivity.this, UnitListActivity.class);

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.events:
                Parameters.category = 54;
                Parameters.categoryTitle = getResources().getString(R.string.categoryEvents);
                startActivity(intent);
                return true;
            case R.id.blogs:
                Parameters.category = 106;
                Parameters.categoryTitle = getResources().getString(R.string.categoryBlogs);
                startActivity(intent);
                return true;
            case R.id.vegan_news:
                Parameters.category = 0;
                Parameters.categoryTitle = getResources().getString(R.string.categoryNews);
                startActivity(intent);
                return true;
            case R.id.advertise:
                webIntent(R.id.advertise);
                return true;
            case R.id.contact:
                webIntent(R.id.contact);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        h.postDelayed(new Runnable() {
            public void run() {
                sliderFunction();
                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    private void createIntent() {
        Intent intent = new Intent(LocationActivity.this, CategoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void webIntent(int id) {
        Intent intent = new Intent(LocationActivity.this, WebViewActivity.class);
        if (id == R.id.advertise) {
            intent.putExtra("link", "http://veganguidegreece.com/advertised/");
        } else if (id == R.id.contact) {
            intent.putExtra("link", "http://veganguidegreece.com/contact-us/");
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void sliderFunction() {
        if (Parameters.slideImageList.size() == 0) {
            Requests.getSliderImages(new Requests.VolleyCallbackSlider() {
                @Override
                public void onSuccess(JSONObject jsonObject) {
                    try {
                        Parameters.slideImageList.clear();
                        JSONObject object = jsonObject.getJSONObject("wp_revslider_slides");
                        JSONArray records = object.getJSONArray("records");
                        for (int i = 0; i < records.length(); i++) {
                            JSONArray array = records.getJSONArray(i);
                            JSONObject fields = new JSONObject(array.get(3).toString());
                            ImageDto image = new ImageDto(fields);

                            Parameters.slideImageList.add(image);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //ToDo Handle sliderException
                    }
                }

                @Override
                public void onError(VolleyError error) {
                }
            });
        } else if (Parameters.slideImageList.size() - 1 > imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue++;
        } else if (Parameters.slideImageList.size() - 1 == imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue = 0;
        } else {
            imageQueue = 0;
        }

        if (imageDto != null)
            if (imageDto.imageUrl != null)
                Glide.with(VeganContext.getContext())
                        .load(imageDto.imageUrl)
                        .into(banner);
    }

    private void adWebView() {
        if (imageDto != null) {
            if (imageDto.imageLink != null && !imageDto.imageLink.isEmpty()) {
                Intent intent = new Intent(LocationActivity.this, WebViewActivity.class);
                intent.putExtra("link", imageDto.imageLink);
                startActivity(intent);
            } else
                Toast.makeText(VeganContext.getContext(), getResources().getString(R.string.unavailable_ad_link), Toast.LENGTH_SHORT).show();
        }
    }

}
