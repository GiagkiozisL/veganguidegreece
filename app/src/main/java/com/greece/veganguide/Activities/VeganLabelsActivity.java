package com.greece.veganguide.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.greece.veganguide.R;
import com.greece.veganguide.RecyclerViewAdapter.LabelAdapter;

import java.util.ArrayList;
import java.util.List;

public class VeganLabelsActivity extends AppCompatActivity {

    List<Integer> listImages = new ArrayList<>();
    Button nextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vegan_labels);

        nextBtn = findViewById(R.id.buttonNext);

        initData();
        HorizontalInfiniteCycleViewPager horizontalInfiniteCycleViewPager = findViewById(R.id.horizontalInfiniteCycleViewPager);
        LabelAdapter labelAdapter = new LabelAdapter(listImages, this);
        horizontalInfiniteCycleViewPager.setAdapter(labelAdapter);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VeganLabelsActivity.this, LocationActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void initData() {
        listImages.add(R.drawable.v);
        listImages.add(R.drawable.vf);
        listImages.add(R.drawable.vv);
    }
}