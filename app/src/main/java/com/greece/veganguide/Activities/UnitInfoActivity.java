package com.greece.veganguide.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.greece.veganguide.Model.DtoHelpers;
import com.greece.veganguide.Model.UnitDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.UnitUtilities;
import com.greece.veganguide.Utilities.VeganContext;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class UnitInfoActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final int REQUEST_PHONE = 1;
    private UnitDto unitDto;
    private ImageView unitImage;
    private ImageView unitMiniLogo;
    private TextView unitMiniTitle;
    private TextView unitMiniCity;
    private TextView unitMiniMoto;
    private TextView description;
    private TextView readMore;
    private TextView address;
    private TextView contactPhone;
    private TextView contactMail;
    private TextView category;
    private ImageView categoryImage;
    private ImageView toggleTimeImg;
    private TextView veganFollow;
    private TextView unitFollow;
    private ImageView facebook;
    private ImageView twitter;
    private ImageView instagram;
    private ImageView google;
    private ImageView veganTag;
    private CardView workCardView;
    private TextView monday;
    private TextView tuesday;
    private TextView wednesday;
    private TextView thursday;
    private TextView friday;
    private TextView saturday;
    private TextView sunday;
    private ConstraintLayout timeLayout;
    private boolean descriptionFlag;
    private boolean workFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageToLoad = "en_US";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        unitImage = findViewById(R.id.unitImage);
        unitMiniLogo = findViewById(R.id.unitPhone);
        unitMiniTitle = findViewById(R.id.unitTitle);
        unitMiniCity = findViewById(R.id.unitCity);
        unitMiniMoto = findViewById(R.id.unitMoto);
        description = findViewById(R.id.description);
        readMore = findViewById(R.id.readMore);
        address = findViewById(R.id.address);
        CardView infoMap = findViewById(R.id.infoMap);
        contactPhone = findViewById(R.id.infoPhoneContact);
        contactMail = findViewById(R.id.infoMailContact);
        category = findViewById(R.id.categoryTitle);
        categoryImage = findViewById(R.id.categoryIcon);
        veganFollow = findViewById(R.id.infoVeganFollow);
        unitFollow = findViewById(R.id.infoUnitFollow);
        facebook = findViewById(R.id.facebookBtn);
        twitter = findViewById(R.id.twitterBtn);
        instagram = findViewById(R.id.instagramBtn);
        google = findViewById(R.id.googleBtn);
        veganTag = findViewById(R.id.vegan_tag);
        workCardView = findViewById(R.id.workCardView);
        timeLayout = findViewById(R.id.unitTimeContainer);
        toggleTimeImg = findViewById(R.id.toggleTimeImageView);
        monday = findViewById(R.id.mondayTime);
        tuesday = findViewById(R.id.tuesdayTime);
        wednesday = findViewById(R.id.wednesdayTime);
        thursday = findViewById(R.id.thursdayTime);
        friday= findViewById(R.id.fridayTime);
        saturday = findViewById(R.id.saturdayTime);
        sunday = findViewById(R.id.sundayTime);
        descriptionFlag = false;
        workFlag = false;
        toggleTimeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down));


        if (Parameters.category == 106)
            infoMap.setVisibility(View.GONE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            unitDto = (UnitDto) bundle.getSerializable("unitDto");
        }

        if (unitDto != null) {
            setUpView();
            socialListeners();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void setUpView() {
        setTitle(Html.fromHtml(unitDto.title));

        Glide.with(VeganContext.getContext())
                .load(unitDto.coverUrl)
                .into(unitImage);

        Glide.with(VeganContext.getContext())
                .load(unitDto.logoUrl)
                .apply(new RequestOptions().centerCrop())
                .into(unitMiniLogo);

        unitMiniTitle.setText(Html.fromHtml(unitDto.getTitle()));
        if (!unitDto.address.isEmpty())
            unitMiniCity.setText(unitDto.address);
        unitMiniMoto.setText(Html.fromHtml(unitDto.getMoto()));


        if (unitDto.vvf != null)
            if (DtoHelpers.getVeganTag(unitDto.vvf) != -1)
            veganTag.setImageDrawable(getResources().getDrawable(DtoHelpers.getVeganTag(unitDto.vvf)));


        if (unitDto.getDescription() != null)
            description.setText(Html.fromHtml(unitDto.getDescription().replace("&nbsp;","<br />")));

        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (descriptionFlag) {
                    description.setEllipsize(TextUtils.TruncateAt.END);
                    description.setMaxLines(2);
                    readMore.setText(getResources().getString(R.string.read_more));
                } else {
                    description.setEllipsize(null);
                    description.setMaxLines(Integer.MAX_VALUE);
                    readMore.setText(getResources().getString(R.string.read_less));
                }
                descriptionFlag = !descriptionFlag;
            }
        });

        workCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workFlag) {
                    timeLayout.setVisibility(View.GONE);
                    toggleTimeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down));
                } else {
                    timeLayout.setVisibility(View.VISIBLE);
                    toggleTimeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_up));
                }
                workFlag = !workFlag;
            }
        });


        category.setText(Parameters.categoryTitle);
        categoryImage.setImageDrawable(getResources().getDrawable(UnitUtilities.DrawableCategoryMap()));


        if (!unitDto.getPhone().isEmpty()) {
            contactPhone.setText(unitDto.getPhone());
            contactPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // For Android 6.0 and above we need Request Permissions on RunTime
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // Here, Check Permission in current activity
                        if (ContextCompat.checkSelfPermission(UnitInfoActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // Request the permission
                            ActivityCompat.requestPermissions(UnitInfoActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE);
                        } else {
                            // Permission has already been granted
                            phoneIntent(unitDto.getPhoneNumber());
                        }
                    } else {
                        phoneIntent(unitDto.getPhoneNumber());
                    }
                }
            });
        }

        if (!unitDto.getMail().isEmpty()) {
            contactMail.setText(unitDto.getMail());
            contactMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mailIntent(contactMail.getText().toString());
                }
            });
        }


        if (!unitDto.getAddress().isEmpty())
            address.setText(Html.fromHtml(unitDto.getAddress()));


        if (unitDto.workHours != null && !unitDto.workHours.isEmpty())
            workHoursRender(unitDto.workHours);
        else
            workCardView.setVisibility(View.GONE);


        veganFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!unitDto.getVeganLink().isEmpty() && unitDto.getVeganLink() != null)
                    webViewRender(unitDto.getVeganLink());
                else
                    Toast.makeText(VeganContext.getContext(), "Web Link is temporary unavailable", Toast.LENGTH_SHORT).show();
            }
        });

        unitFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!unitDto.getUnitLink().isEmpty() && unitDto.getUnitLink() != null)
                    webViewRender(unitDto.getUnitLink());
                else
                    Toast.makeText(VeganContext.getContext(), "Web Page is unavailable", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void socialListeners() {
        try {
            final JSONArray jsonArray = new JSONArray(unitDto.getSocialLinks());

            final String socialFacebookLink = DtoHelpers.getLinkByTag("Facebook", jsonArray);
            if (socialFacebookLink != null) {
                facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        socialIntent(socialFacebookLink);
                    }
                });
            } else {
                facebook.setVisibility(View.GONE);
            }

            final String socialTwitterLink = DtoHelpers.getLinkByTag("Twitter", jsonArray);
            if (socialTwitterLink != null) {
                twitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        socialIntent(socialTwitterLink);
                    }
                });
            } else {
                twitter.setVisibility(View.GONE);
            }

            final String socialInstagramLink = DtoHelpers.getLinkByTag("Instagram", jsonArray);
            if (socialInstagramLink != null) {
                instagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        socialIntent(socialInstagramLink);
                    }
                });
            } else {
                instagram.setVisibility(View.GONE);
            }

            final String socialGoogleLink = DtoHelpers.getLinkByTag("Google", jsonArray);
            if (socialGoogleLink != null) {
                google.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        socialIntent(socialGoogleLink);
                    }
                });
            } else {
                google.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void phoneIntent(String phoneNumber) {
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(Intent.createChooser(intent, "Call Using..."));
        } else
            Toast.makeText(VeganContext.getContext(), "Phone Number temporary unavailable ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted, yay!
                    Toast.makeText(VeganContext.getContext(), "Phone Permission Granted!", Toast.LENGTH_SHORT).show();
                    phoneIntent(unitDto.getPhoneNumber());
                } else {
                    // permission denied, boo!
                    Toast.makeText(VeganContext.getContext(), "Phone Permission Denied! Go to Settings to change permission...", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void mailIntent(String mailAddress) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailAddress});
        emailIntent.setType("text/plain");
        startActivity(emailIntent);
    }

    private void socialIntent(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    private void webViewRender(String URL) {
        Intent intent = new Intent(UnitInfoActivity.this, WebViewActivity.class);
        intent.putExtra("link", URL);
        startActivity(intent);
    }

    private void workHoursRender(String workHours) {
        try {
            JSONObject works = new JSONObject(workHours);
            monday.setText(works.getString("Monday").replace(",", "-"));
            tuesday.setText(works.getString("Tuesday").replace(",", "-"));
            wednesday.setText(works.getString("Wednesday").replace(",", "-"));
            thursday.setText(works.getString("Thursday").replace(",", "-"));
            friday.setText(works.getString("Friday").replace(",", "-"));
            saturday.setText(works.getString("Saturday").replace(",", "-"));
            sunday.setText(works.getString("Sunday").replace(",", "-"));

        } catch (Exception ignored) {
            workCardView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng unitLocation;
        if (unitDto.lat != null && unitDto.lng != null)
            unitLocation = new LatLng(unitDto.lat, unitDto.lng);
        else
            unitLocation = UnitUtilities.getLocationFromAddress(VeganContext.getContext(), unitDto.address);

        MarkerOptions markerOptions;
        if (unitLocation != null) {
            markerOptions = new MarkerOptions().position(unitLocation).title(unitMiniTitle.getText().toString());
            googleMap.addMarker(markerOptions).showInfoWindow();
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(unitLocation));
        } else {
            markerOptions = new MarkerOptions().position(new LatLng(0.0, 0.0)).title("Address not found!");
            googleMap.addMarker(markerOptions).showInfoWindow();
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(0.0, 0.0)));
        }
    }
}

