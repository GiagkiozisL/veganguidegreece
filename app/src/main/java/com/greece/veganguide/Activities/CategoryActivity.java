package com.greece.veganguide.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.greece.veganguide.Model.ImageDto;
import com.greece.veganguide.R;
import com.greece.veganguide.Utilities.Parameters;
import com.greece.veganguide.Utilities.Requests;
import com.greece.veganguide.Utilities.UnitUtilities;
import com.greece.veganguide.Utilities.VeganContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CategoryActivity extends AppCompatActivity {

    Handler h = new Handler();
    int delay = 5 * 1000;
    Runnable runnable;
    private int category;
    private int imageQueue = 0;
    private ImageView banner;
    private ImageDto imageDto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        setTitle(R.string.category);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        View categoryEat = findViewById(R.id.categoryEat);
        View categoryShop = findViewById(R.id.categoryShop);
        View categoryEshop = findViewById(R.id.categoryEshop);
        View categoryClean = findViewById(R.id.categoryClean);
        View categoryEvents = findViewById(R.id.categoryEvents);
        View categoryBlogs = findViewById(R.id.categoryBlogs);
        View categoryDoctors = findViewById(R.id.categoryDoctors);
        View categoryNews = findViewById(R.id.categoryNews);
        banner = findViewById(R.id.categoryBanner);


        categoryEat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = 50;
                Parameters.categoryTitle = getResources().getString(R.string.categoryEat);
                createIntent(category);
            }
        });

        categoryShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = 51;
                Parameters.categoryTitle = getResources().getString(R.string.categoryShop);
                createIntent(category);
            }
        });

        categoryEshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = 52;
                Parameters.categoryTitle = getResources().getString(R.string.categoryEshop);
                createIntent(category);
            }
        });

        categoryClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = 53;
                Parameters.categoryTitle = getResources().getString(R.string.categoryClean);
                createIntent(category);
            }
        });

//        categoryBlogs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                category = 106;
//                Parameters.categoryTitle = getResources().getString(R.string.categoryBlogs);
//                createIntent(category);
//            }
//        });
//
//        categoryEvents.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                category = 54;
//                Parameters.categoryTitle = getResources().getString(R.string.categoryEvents);
//                createIntent(category);
//            }
//        });

        categoryDoctors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = 56;
                Parameters.categoryTitle = getResources().getString(R.string.categoryDoctors);
                createIntent(category);
            }
        });

//        categoryNews.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                category = 0;
//                Parameters.categoryTitle = getResources().getString(R.string.categoryNews);
//                createIntent(category);
//            }
//        });

        sliderFunction();

        banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adWebView();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.events:
                category = 54;
                Parameters.categoryTitle = getResources().getString(R.string.categoryEvents);
                createIntent(category);
                return true;
            case R.id.blogs:
                category = 106;
                Parameters.categoryTitle = getResources().getString(R.string.categoryBlogs);
                createIntent(category);
                return true;
            case R.id.vegan_news:
                category = 0;
                Parameters.categoryTitle = getResources().getString(R.string.categoryNews);
                createIntent(category);
                return true;
            case R.id.advertise:
                webIntent(R.id.advertise);
                return true;
            case R.id.contact:
                webIntent(R.id.contact);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        h.postDelayed(new Runnable() {
            public void run() {
                sliderFunction();
                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    private void createIntent(int categoryId) {
        Parameters.category = categoryId;
        Intent intent = new Intent(CategoryActivity.this, UnitListActivity.class);
        startActivity(intent);
    }

    private void webIntent(int id) {
        Intent intent = new Intent(CategoryActivity.this, WebViewActivity.class);
        if (id == R.id.advertise) {
            intent.putExtra("link", "http://veganguidegreece.com/advertised/");
        } else if (id == R.id.contact) {
            intent.putExtra("link", "http://veganguidegreece.com/contact-us/");
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CategoryActivity.this, LocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void sliderFunction() {
        if (Parameters.slideImageList.size() == 0) {
            Requests.getSliderImages(new Requests.VolleyCallbackSlider() {
                @Override
                public void onSuccess(JSONObject jsonObject) {
                    try {
                        Parameters.slideImageList.clear();
                        JSONObject object = jsonObject.getJSONObject("wp_revslider_slides");
                        JSONArray records = object.getJSONArray("records");
                        for (int i = 0; i < records.length(); i++) {
                            JSONArray array = records.getJSONArray(i);
                            JSONObject fields = new JSONObject(array.get(3).toString());
                            ImageDto image = new ImageDto(fields);

                            Parameters.slideImageList.add(image);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //ToDo Handle sliderException
                    }
                }

                @Override
                public void onError(VolleyError error) {
                }
            });
        } else if (Parameters.slideImageList.size() - 1 > imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue++;
        } else if (Parameters.slideImageList.size() - 1 == imageQueue) {
            imageDto = UnitUtilities.getSliderImageUrl(imageQueue);
            imageQueue = 0;
        } else {
            imageQueue = 0;
        }

        if (imageDto != null)
            if (imageDto.imageUrl != null)
                Glide.with(VeganContext.getContext())
                        .load(imageDto.imageUrl)
                        .into(banner);
    }

    private void adWebView() {
        if (imageDto != null) {
            if (imageDto.imageLink != null && !imageDto.imageLink.isEmpty()) {
                Intent intent = new Intent(CategoryActivity.this, WebViewActivity.class);
                intent.putExtra("link", imageDto.imageLink);
                startActivity(intent);
            } else
                Toast.makeText(VeganContext.getContext(), getResources().getString(R.string.unavailable_ad_link), Toast.LENGTH_SHORT).show();
        }
    }

}
